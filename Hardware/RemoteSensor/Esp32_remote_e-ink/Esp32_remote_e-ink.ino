/*
 Name:		Esp32_remote_e_ink.ino
 Created:	28.12.2018 16:52:02
 Author:	mgr
*/

#include <WiFi.h>
#include "Adafruit_MQTT_Client.h"
#include "DHT.h"
#include <SPI.h>
#include "epd2in13.h"
#include "epdpaint.h"

#include <esp_wifi.h>
#include <esp_bt.h>

#define SLEEP_TIME 300e6
//#define SLEEP_TIME 5e6
#define DEBUG true
#define COLORED     0
#define UNCOLORED   1

/************************* Temp sensor config ********************************/
#define DHTPIN 21
#define DHTTYPE DHT22
/************************* WiFi Access Point *********************************/
#define WLAN_SSID       "mgr"
#define WLAN_PASS       ""
/************************* Adafruit.io Setup *********************************/
#define MQTT_SERVER      "192.168.1.111"
#define MQTT_SERVERPORT  1883                   // use 8883 for SSL
#define ENDPOINT_NAME    "sypialnia"
/************ Global State (you don't need to change this!) ******************/
RTC_DATA_ATTR float temp = 0;
RTC_DATA_ATTR float humi = 0;

char tbs[16];
unsigned char image[32000];
Paint paint(image, 0, 0);

DHT dht(DHTPIN, DHTTYPE);
WiFiClient client;
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, MQTT_SERVERPORT);
Adafruit_MQTT_Publish sensor = Adafruit_MQTT_Publish(&mqtt, "ohc/sensors/" ENDPOINT_NAME);
Epd epd;

void MQTT_connect();
void Publish();
void DebugPrint(const char * text);
void DebugPrintln(const char *text);

void setup() {
#if DEBUG
	Serial.begin(115200);
	Serial.println();
#endif

	esp_bt_controller_disable();

	if (temp == 0)
	{
		DebugPrintln("Full update");
		// First boot, clear entire display
		if (epd.Init(lut_full_update) != 0) {
			DebugPrintln("e-Paper init failed");
			return;
		}

		epd.ClearFrameMemory(0xFF);   // bit set = white, bit reset = black
		epd.DisplayFrame();
		epd.ClearFrameMemory(0xFF);   // bit set = white, bit reset = black
		epd.DisplayFrame();
	}
	else
	{
		if (epd.Init(lut_partial_update) != 0) {
			DebugPrintln("e-Paper init failed");
			return;
		}
	}


	dht.begin();

	int counter = 0;
	float h, t;
	do
	{
		h = dht.readHumidity();
		t = dht.readTemperature();

		// Check if any reads failed and exit early (to try again).
		if (isnan(h) || isnan(t)) {
			DebugPrintln("Failed to read from DHT sensor!");
			delay(2500);
		}

		counter++;
		if (counter >= 100)
		{
			// Die and wait for WDT reset.
			while (true);
		}
	} while ((isnan(h) || isnan(t)));

	sprintf(tbs, "%.1f �C %.f %%", t, h);
	DebugPrint("\nCurrent sensor values: ");
	DebugPrintln(tbs);

	if (temp != t || humi != h)
	{
	    temp = t;
		humi = h;

		DebugPrintln("Need update and upload...");
		paint.SetRotate(ROTATE_0);
		paint.SetWidth(120);    // width should be the multiple of 8 
		paint.SetHeight(250);
		paint.Clear(UNCOLORED);
		paint.SetRotate(ROTATE_90);

		sprintf(tbs, "Temp: %.1f*C", t);
		paint.DrawStringAt(0, 10, tbs, &Font24, COLORED);

		sprintf(tbs, "Humidity: %.f%%", h);
		paint.DrawStringAt(0, 48, tbs, &Font24, COLORED);

		epd.SetFrameMemory(paint.GetImage(), 0, 0, paint.GetWidth(), paint.GetHeight());
		epd.DisplayFrame();
		epd.SetFrameMemory(paint.GetImage(), 0, 0, paint.GetWidth(), paint.GetHeight());
		epd.DisplayFrame();

		Publish();
	}

	DebugPrintln("Setup ESP32 to sleep...");
	delay(500);

	esp_sleep_enable_timer_wakeup(SLEEP_TIME);
	esp_deep_sleep_start();
	Serial.println("This will never be printed");
}

void loop() 
{
}

void Publish()
{
	//esp_wifi_start();
	WiFi.mode(WIFI_STA);
	//esp_wifi_start();
	WiFi.begin(WLAN_SSID, WLAN_PASS);
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
#if DEBUG
		Serial.print(".");
#endif
	}
#if DEBUG
	Serial.println();
	Serial.println("WiFi connected");
#endif

	MQTT_connect();

	sprintf(tbs, "%f;%f", temp, humi);

#if DEBUG
	Serial.print(F("\nSending sensor val "));
	Serial.print(tbs);
	Serial.print("...");
#endif
	if (!sensor.publish(tbs)) {
#if DEBUG
		Serial.println(F("Failed"));
#endif
	}
	else {
#if DEBUG
		Serial.println(F("OK!"));
#endif
	}

	mqtt.disconnect();
	WiFi.disconnect(true);
	WiFi.mode(WIFI_OFF);
	esp_wifi_stop();
}


// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
	int8_t ret;

	// Stop if already connected.
	if (mqtt.connected()) {
		return;
	}

#if DEBUG
	Serial.print("Connecting to MQTT... ");
#endif
	uint8_t retries = 15;
	while ((ret = mqtt.connect()) != 0) {
#if DEBUG
		Serial.println(mqtt.connectErrorString(ret));
		Serial.println("Retrying MQTT connection in 5 seconds...");
#endif
		mqtt.disconnect();
		delay(5000);
		retries--;
		if (retries == 0) {
			// basically die and wait for WDT to reset me
			while (1);
		}
	}

#if DEBUG
	Serial.println("MQTT Connected!");
#endif
}


void DebugPrint(const char *text)
{
#if DEBUG
	Serial.print(text);
#endif
}
void DebugPrintln(const char *text)
{
#if DEBUG
	Serial.println(text);
#endif
}
