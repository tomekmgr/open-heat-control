#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include "DHT.h"


#define DHTPIN 2     // what digital pin we're connected to
#define DHTTYPE DHT11   // DHT 11
/************************* WiFi Access Point *********************************/
#define WLAN_SSID       "mgr"
#define WLAN_PASS       "witamiokluczpytam"
/************************* Adafruit.io Setup *********************************/
#define MQTT_SERVER      "192.168.1.111"
#define MQTT_SERVERPORT  1883                   // use 8883 for SSL
#define ENDPOINT_NAME    "sypialnia"
/************ Global State (you don't need to change this!) ******************/
WiFiClient client;
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, MQTT_SERVERPORT);
Adafruit_MQTT_Publish sensor = Adafruit_MQTT_Publish(&mqtt, "ohc/sensors/" ENDPOINT_NAME);
/*************************** Sketch Code ************************************/

// Bug workaround for Arduino 1.6.6, it seems to need a function declaration
// for some reason (only affects ESP8266, likely an arduino-builder bug).
void MQTT_connect();
DHT dht(DHTPIN, DHTTYPE);
uint32_t calculateCRC32(const uint8_t *data, size_t length);
void printMemory();
void LoadValues();
void StoreValues();
void Publish();

union content
{
  byte data[8];
  struct {
    float temp;
    float humi;  
  } values;
};

struct {
  uint32_t crc32;
  content data;
} rtcData;



void setup() {
  Serial.begin(115200);
  Serial.println();
  //TODO ---- Ultra REMOVE ----
  delay(10000);

  LoadValues();

  dht.begin();

  float h, t;
  do
  {
      h = dht.readHumidity();
      t = dht.readTemperature();
      
      // Check if any reads failed and exit early (to try again).
      if (isnan(h) || isnan(t)) {
        Serial.println("Failed to read from DHT sensor!");
        delay(1000);
      }
  } while((isnan(h) || isnan(t)));

  char tbs[16];

  sprintf(tbs, "%f;%f", t, h);
  // Now we can publish stuff!
  Serial.print(F("\nSending sensor val "));
  Serial.println(tbs);

  if(rtcData.data.values.temp != t || rtcData.data.values.humi != h)
  {
      rtcData.data.values.temp = t;
      rtcData.data.values.humi = h;
      StoreValues();

      Publish();
  }

  Serial.println("Going into deep sleep for 5 minutes");
  ESP.deepSleep(300e6);
}

void loop() {
  // put your main code here, to run repeatedly:

}


uint32_t calculateCRC32(const uint8_t *data, size_t length) {
  uint32_t crc = 0xffffffff;
  while (length--) {
    uint8_t c = *data++;
    for (uint32_t i = 0x80; i > 0; i >>= 1) {
      bool bit = crc & 0x80000000;
      if (c & i) {
        bit = !bit;
      }
      crc <<= 1;
      if (bit) {
        crc ^= 0x04c11db7;
      }
    }
  }
  return crc;
}

void printMemory() {
  char buf[3];
  uint8_t *ptr = (uint8_t *)&rtcData;
  for (size_t i = 0; i < sizeof(rtcData); i++) {
    sprintf(buf, "%02X", ptr[i]);
    Serial.print(buf);
    if ((i + 1) % 32 == 0) {
      Serial.println();
    } else {
      Serial.print(" ");
    }
  }
  Serial.println();
}

void LoadValues()
{
  if (ESP.rtcUserMemoryRead(0, (uint32_t*) &rtcData, sizeof(rtcData))) 
  {
    Serial.println();
    uint32_t crcOfData = calculateCRC32((uint8_t*) &rtcData.data.data[0], sizeof(rtcData.data));
    Serial.print("Temp: ");
    Serial.println(rtcData.data.values.temp, HEX);
    Serial.print("Humi: ");
    Serial.println(rtcData.data.values.humi, HEX);
    if (crcOfData != rtcData.crc32) {
      Serial.println("CRC32 in RTC memory doesn't match CRC32 of data. Data is probably invalid!");
      rtcData.data.values.temp = 0;
      rtcData.data.values.humi = 0;
    } else {
      Serial.println("CRC32 check ok, data is probably valid.");
    }
  }
  else
  {
      // Cannot read stored data, start with defaults.
      // TODO: Show error on screen.  
      rtcData.data.values.temp = 0
      rtcData.data.values.humi = 0;
  }
}

void StoreValues()
{
    rtcData.crc32 = calculateCRC32((uint8_t*) &rtcData.data.data[0], sizeof(rtcData.data));
    ESP.rtcUserMemoryWrite(0, (uint32_t*) &rtcData, sizeof(rtcData));
}

void Publish()
{
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.println("WiFi connected");
  MQTT_connect();
  
  char tbs[16];
  sprintf(tbs, "%f;%f", rtcData.data.values.temp, rtcData.data.values.humi);
  // Now we can publish stuff!
  Serial.print(F("\nSending sensor val "));
  Serial.print(tbs);
  Serial.print("...");
  if (!sensor.publish(tbs)) {
    Serial.println(F("Failed"));
  } else {
    Serial.println(F("OK!"));
  }

  mqtt.disconnect();
  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
  //delay(3000);
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}
