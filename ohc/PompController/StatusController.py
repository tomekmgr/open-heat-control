import os
import time
import pathlib
import config
import w1thermsensor

import Adafruit_Nokia_LCD as LCD
import Adafruit_GPIO.SPI as SPI

from threading import Event
from gpiozero import LED
from gpiozero import CPUTemperature
from paho.mqtt.client import Client
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

# Raspberry Pi hardware SPI config:
DC = 23
RST = 24
SPI_PORT = 0
SPI_DEVICE = 0
backlight = LED(22)

rel1 = "n/a"
rel2 = "n/a"
auxTemp = 0.0

updateEvent = Event()


def update(draw: ImageDraw, disp: LCD.PCD8544):
    global rel1
    global rel2
    global auxTemp

    draw.rectangle((0, 0, LCD.LCDWIDTH, LCD.LCDHEIGHT), outline=255, fill=255) # clear
    draw.rectangle((0, 0, LCD.LCDWIDTH - 1, LCD.LCDHEIGHT - 1), outline=0, fill=255) # draw border
    draw.line((0, LCD.LCDHEIGHT / 2, LCD.LCDWIDTH, LCD.LCDHEIGHT / 2))
    draw.line((LCD.LCDWIDTH / 2, LCD.LCDHEIGHT / 2, LCD.LCDWIDTH / 2, LCD.LCDHEIGHT))
   
    # top row
    draw.text((2, 2), "CPU: %.1f °C" % CPUTemperature().temperature)
    draw.text((2, 12), "AUX: %.1f °C" % auxTemp)

    # left cell
    draw.text((2, (LCD.LCDHEIGHT / 2) + 2), " REL1:")
    draw.text((2, (LCD.LCDHEIGHT / 2) + 12), " " + rel1)

    # right cell
    draw.text(((LCD.LCDWIDTH / 2) + 2, (LCD.LCDHEIGHT / 2) + 2), " REL2:")
    draw.text(((LCD.LCDWIDTH / 2) + 2, (LCD.LCDHEIGHT / 2) + 12), " " + rel2)

    disp.image(image)
    disp.display()


def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("ohc/#")


def on_message(client, userdata, msg):
    global rel1
    global rel2
    global updateEvent
    value = str(msg.payload.decode("utf-8"))
    if msg.topic == "ohc/floorpomp":
        rel1 = value
        updateEvent.set()
    elif msg.topic == "ohc/auxpomp":
        rel2 = value
        updateEvent.set()


def on_disconnect(client, userdata, rc):
    global rel1
    global rel2
    global disp
    global draw

    rel1 = "n/a"
    rel2 = "n/a"
    while True:
        update(draw, disp)
        try:
            if client.reconnect() == 0:
                return

            time.sleep(0.5)
        except:
            pass


def connect():
    global client
    try:
        print("Connecting to broker...")
        return client.connect(config.MqttBroker)
    except OSError as e:
        print(e)
        return -1


def get_aux_temp():
    global auxTemp
    auxTemp = w1thermsensor.W1ThermSensor().get_temperature()


# Hardware SPI usage:
disp = LCD.PCD8544(DC, RST, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE, max_speed_hz=4000000))
disp.begin(contrast=52)
disp.clear()
disp.display()
backlight.on()

image = Image.new('1', (LCD.LCDWIDTH, LCD.LCDHEIGHT))
draw = ImageDraw.Draw(image)

os.chdir(str(pathlib.Path.home()))
if pathlib.Path('.rel1').exists():
    with open('.rel1', 'r') as file:
        rel1 = file.read()
        
if pathlib.Path('.rel2').exists():
    with open('.rel2', 'r') as file:
        rel2 = file.read()

update(draw, disp)

client = Client()
client.on_connect = on_connect
client.on_message = on_message
client.on_disconnect = on_disconnect

while True:
    status = connect()
    update(draw, disp)
    if status == 0:
        break
    time.sleep(1.0)

while True:
    get_aux_temp()
    try:
        client.publish('ohc/sensors/water temp', '{temp};0.0'.format(temp=auxTemp))
    except:
        pass

    update(draw, disp)
    try:
        client.loop()
    except:
        pass
    updateEvent.wait(2.0)
    updateEvent.clear()

backlight.off()
disp.clear()
disp.display()
del disp
