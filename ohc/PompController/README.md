# Pomp controller

# Hardware
* Raspberry PI ZERO (WiFi) 
* Nokia LCD 5110/3110 84x48px
* Two channel relay module 10A/250V (optoisolated)
* DS18B20 temperature sensor

## Wiring
Raspberry PI Zero header (from top)

| connection | pin | pin | connection |
|--|--|--|--|
|LCD VCC +3.3|1|2||
||3|4|Relay VCC +5|
||5|6|LCD GND|
|DS18B20 data|7|8||
|Relay GND|9|10||
|Relay CH1|11|12|Relay CH2|
||13|14|DS18B20 GND|
|LCD backlight|15|16|LCD D/C|
|DS18B20 VCC|17|18|LCD RST|
|LCD DIN(mosi)|19|20||
||21|22||
|LCD SCLK|23|24|LCD SCE|
||25|26||
||27|28||
||29|30||
||31|32||
||33|34||
||35|36||
||37|38||
||39|40||
