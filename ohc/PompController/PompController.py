import os
import sys
import time
import pathlib
import config
from threading import Timer
from gpiozero import *
from paho.mqtt.client import *

'''
Standard 

'''
floorpompLock = False
auxpompLock = False
'''
'''

rel1 = OutputDevice(17, False)
rel2 = OutputDevice(18, False)

def clearLock(lock):
    global floorpompLock
    global auxpompLock

    if lock == "floorpompLock":
        floorpompLock = False
    elif lock == "auxpompLock":
        auxpompLock = False

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    #client.subscribe("#") #ohc/floorpomp
    client.subscribe("ohc/#")

def on_message(client, userdata, msg):
    value = str(msg.payload.decode("utf-8"))
    if msg.topic == "ohc/floorpomp":
        floorpompHandler(value)
    elif msg.topic == "ohc/auxpomp":
        auxpompHandler(value)

def floorpompHandler(msg):
    global floorpompLock
    global rel1
    print("Handling floor pomp...", end='')
    if not floorpompLock:
        with open('.rel1', 'w') as file: 
            file.write(msg)
        if msg == "on":
            rel1.on()
            floorpompLock = True
            Timer(30, clearLock, {"floorpompLock"}).start()
        elif msg == "off":
            rel1.off()
        print("[Done]\nPomp handled with state " + msg)
    else:
        print("[Failed]\nPomp is locked")

def auxpompHandler(msg):
    global auxpompLock
    global rel2
    print("Handling aux pomp...", end='')
    if not auxpompLock:
        with open('.rel2', 'w') as file: 
            file.write(msg)
        if msg == "on":
            rel2.on()
            auxpompLock = True
            Timer(30, clearLock, {"auxpompLock"}).start()
        elif msg == "off":
            rel2.off()
        print("[Done]\nPomp handled with state " + msg)
    else:
        print("[Failed]\nPomp is locked")

def connect():
    global client
    try:
        print("Connecting to broker...")
        return client.connect(config.MqttBroker)
    except OSError as e:
        print(e)
        return -1

os.chdir(str(pathlib.Path.home()))
if pathlib.Path('.rel1').exists() :
    #file exists, read and decide what to do.
    print("Status file for REL1 was found.")
    with open('.rel1', 'r') as file:
        action = file.read()
        floorpompHandler(action)
else :
    print("Status file for REL2 NOT found.")

if pathlib.Path('.rel2').exists() :
    #file exists, read and decide what to do.
    print("Status file for REL2 was found.")
    with open('.rel2', 'r') as file:
        action = file.read()
        auxpompHandler(action)
else:
    print("Status file for REL2 NOT found.")

client = Client()
client.on_connect = on_connect
client.on_message = on_message

while True:
    status = connect()
    if status == 0:
        break

try:
    print("MQTT broker connected, waiting for events")
    client.loop_forever()
except KeyboardInterrupt:
    print("CTRL+C, leaving program")
    del rel1
    del rel2
