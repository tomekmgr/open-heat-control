import pytest
import TempZone
import logging

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger()


class FakeServo(object):

    def __init__(self):
        self.TurnedOn = False

    def on(self):
        self.TurnedOn = True

    def off(self):
        self.TurnedOn = False


def test_simple():
    """Test if zone without update is heating"""
    zone = TempZone.TempZone('Test', 'ohc/sensors/test')
    assert zone.IsHeating is False, "New zone should not heat"


def test_update_to_target():
    """Test if zone with update is heating"""
    zone = TempZone.TempZone('Test', 'ohc/sensors/test')
    zone.Target = 15.0
    zone.update(15.0, 15.0)
    assert zone.IsHeating is False, "Zone with update to target should not heat"


def test_update_below_target():
    """Test if zone with update is heating"""
    zone = TempZone.TempZone('Test', 'ohc/sensors/test')
    zone.Target = 15.0
    zone.update(10.0, 15.0)
    assert zone.IsHeating is True, "Zone with update below target should heat"


def test_update_above_target():
    """Test if zone with update is heating"""
    zone = TempZone.TempZone('Test', 'ohc/sensors/test')
    zone.Target = 15.0
    zone.update(20.0, 15.0)
    assert zone.IsHeating is False, "Zone with update above target should not heat"


def test_hysteresis_down():
    zone = TempZone.TempZone('Test', 'ohc/sensors/test')
    zone.Target = 15.0
    zone.update(15.0, 15.0)
    assert zone.IsHeating is False, "To target"
    zone.update(14.9, 15.0)
    assert zone.IsHeating is False, "Below, 14.9"
    zone.update(14.7, 15.0)
    assert zone.IsHeating is False, "Below, 14.7"
    zone.update(14.5, 15.0)
    assert zone.IsHeating is False, "Below, 14.5"
    zone.update(14.4, 15.0)
    assert zone.IsHeating is True, "Below, 14.4"


def test_hysteresis_up():
    zone = TempZone.TempZone('Test', 'ohc/sensors/test')
    zone.Target = 15.0
    zone.update(15.0, 15.0)
    assert zone.IsHeating is False, "To target"
    zone.update(14.0, 15.0)
    assert zone.IsHeating is True, "Below, 14.0"
    zone.update(14.7, 15.0)
    assert zone.IsHeating is True, "Below, 14.7"
    zone.update(14.8, 15.0)
    assert zone.IsHeating is False, "Below, 14.8"
    zone.update(15.0, 15.0)
    assert zone.IsHeating is False, "To target"


def test_hysteresis_up_rapid():
    zone = TempZone.TempZone('Test', 'ohc/sensors/test')
    zone.Target = 15.0
    zone.update(15.0, 15.0)
    assert zone.IsHeating is False, "To target"
    zone.update(14.0, 15.0)
    assert zone.IsHeating is True, "Below, 14.0"
    zone.update(14.7, 15.0)
    assert zone.IsHeating is True, "Below, 14.7"
    zone.update(19.8, 15.0)
    assert zone.IsHeating is False, "Below, 14.8"
    zone.update(15.0, 15.0)
    assert zone.IsHeating is False, "To target"
    zone.update(15.0, 15.0)
    assert zone.IsHeating is False, "To target"


def test_simple_with_servo():
    zone = TempZone.TempZone('Test', 'ohc/sensors/test')
    zone.Target = 15.0
    s = FakeServo()
    zone.Servos = {s}

    assert s.TurnedOn is False
    zone.update(10, 0)
    assert s.TurnedOn is True
