import os
os.environ["PIGPIO_ADDR"] = "192.168.1.111"
os.environ["SDL_FBDEV"] = "/dev/fb1"
# Uncomment if you have a touch panel and find the X value for your device
# os.environ["SDL_MOUSEDRV"] = "TSLIB"
# os.environ["SDL_MOUSEDEV"] = "/dev/input/eventX"

MqttBroker = "192.168.1.111"


try:
    from gpiozero import OutputDevice
    from TempZone import TempZone
    import logging

    zone1 = TempZone("Zone 1", "ohc/sensors/sypialnia")
    zone1.Servos = {OutputDevice(18, False), OutputDevice(27, False)}
    zone1.Target = 18.0

    zone2 = TempZone("Zone 2", "ohc/sensors/bawialnia")
    zone2.Servos = {OutputDevice(21, False)}
    zone2.Target = 25.0

    zone3 = TempZone("Zone 3", "ohc/sensors/biblioteka")
    zone3.Servos = {OutputDevice(20, False)}
    zone3.Target = 22.0

    TempZones = {zone1, zone2, zone3}
except:
    pass