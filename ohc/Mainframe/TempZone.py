import time
import logging


class TempZone(object):
    def __init__(self, name, sensor_name):
        self.Name = name
        self.Servos = {}
        self.SensorName = sensor_name
        self._is_heating = False
        self.Target = 21.0
        self._currentTemp = 0.0
        self._up_hysteresis = 0.2
        self._down_hysteresis = 0.5
        self.selected_hysteresis = self._up_hysteresis
        self.timestamp = time.monotonic()

    @property
    def IsHeating(self):
        # Zone need to expire when there is no reading,
        # there cannot be situation when sensor is out, and the zone is still heating.
        if time.monotonic() - self.timestamp > 600.0:
            # data older than 15 seconds, data timeout
            logging.error("Sensor {name} timeout".format(name=self.SensorName))
            self._is_heating = False
            self._currentTemp = 0.0
            for servo in self.Servos:
                servo.off()

        return self._is_heating

    def update(self, temp: float, humi=0.0):
        logging.debug("Updating zone: %s temp %f target: %f" % (self.Name, temp, self.Target))
        self.timestamp = time.monotonic()
        if self._currentTemp <= temp:
            # trend is to drop temperature, room will be cooled
            self.selected_hysteresis = self._up_hysteresis
        else:
            self.selected_hysteresis = self._down_hysteresis

        self._currentTemp = temp

        # todo add hysteresis
        if self._currentTemp < (self.Target - self.selected_hysteresis):
            self._is_heating = True
            logging.debug("Zone: %s is heating" % self.Name)
            for servo in self.Servos:
                servo.on()
        else:
            self._is_heating = False
            for servo in self.Servos:
                servo.off()

    def __repr__(self):
        return {
            'name': self.Name,
            'target': self.Target,
            'value': self._currentTemp,
            'is_heating': self.IsHeating,
            'ohc_sensor': self.SensorName,
        }
