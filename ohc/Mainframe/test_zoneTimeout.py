import TempZone
import logging
import time

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger()


def test_simple():
    """Test if zone without update is heating"""
    zone = TempZone.TempZone('Test', 'ohc/sensors/test')
    assert zone.IsHeating is False, "New zone should not heat"


def test_small_timeout():
    zone = TempZone.TempZone('Test', 'ohc/sensors/test')
    zone.Target = 21
    zone.update(10)
    assert zone.IsHeating is True
    time.sleep(5.2)
    assert zone.IsHeating is False
