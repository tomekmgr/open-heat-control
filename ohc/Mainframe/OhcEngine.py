import time
import logging

from logging.handlers import SysLogHandler
from paho.mqtt.client import Client

logger = logging.getLogger()
formatter = logging.Formatter(fmt='[OHC] %(levelname)s :: %(module)s: %(message)s', datefmt='%m-%d-%Y %H:%M:%S')
syslog = SysLogHandler(address='/dev/log')
syslog.setFormatter(formatter)
console = logging.StreamHandler()
console.setFormatter(formatter)
logger.addHandler(console)
logger.addHandler(syslog)
logger.setLevel(logging.DEBUG)
logger.propagate = True

import config


class PompEngine(object):
    def __init__(self, client: Client, delayCounter: int):
        self.client = client
        self.delayCounter = delayCounter
        self.counter = 0
        self.IsOn = False

    def _switchOn(self):
        self.client.publish("ohc/floorpomp", "on")
        self.IsOn = True
        self.counter = self.delayCounter
        logger.debug("Switching pomp ON")
    
    def _switchOff(self):
        self.client.publish("ohc/floorpomp", "off")
        self.IsOn = False
        self.counter = self.delayCounter
        logger.debug("Switching pomp OFF")
    
    def OnIfNeeded(self):
        if self.IsOn:
            # engine is on, follow counter
            self.counter -= 1
            if self.counter <= 0:
                self._switchOn()
        else:
            self._switchOn()
    
    def OffIfNeeded(self):
        if not self.IsOn:
            # engine is off, follow counter
            self.counter -= 1
            if self.counter <= 0:
                self._switchOff()
        else:
            self._switchOff()


def on_connect(client, userdata, flags, rc):
    logger.debug("Connected with result code "+str(rc))
    client.subscribe("ohc/sensors/#")

def on_message(cli, userdata, msg):
    global client
    value = str(msg.payload.decode("utf-8"))
    logger.debug(msg.topic + " " + value)

    temp = float(value.split(';')[0])
    humi = float(value.split(';')[1])
    for zone in config.TempZones:
        if msg.topic == zone.SensorName:
            zone.update(temp, humi)
       

if __name__ == "__main__":
    client = Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(config.MqttBroker)

    pompEngine = PompEngine(client, 10)

    counter = 0
    while True:
        counter += 1
        client.loop()
        if any(zone.IsHeating is True for zone in config.TempZones):
            pompEngine.OnIfNeeded()
        else:
            pompEngine.OffIfNeeded()

        if counter > 20:
            counter = 0
            # publish additional zone information's
            for zone in config.TempZones:
                client.publish('ohc/sensor_info', str(zone.__repr__()))

        time.sleep(1.0)
