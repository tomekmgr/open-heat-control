import pygame
import os
import time
import config
import ast
from collections import namedtuple

from pygame.locals import *
from paho.mqtt.client import Client

Location = namedtuple('Location', ['x', 'y'])

os.chdir(os.path.dirname(os.path.abspath(__file__)))

pygame.init()

# set up the window
disp = pygame.display.set_mode((480, 320), 0, 32)
pygame.display.set_caption('Drawing')
# disable mouse cursor
pygame.mouse.set_visible(False)

widgets = []

# define font
font = pygame.font.Font(None, 25)

# set up the colors
BLACK = (0,   0,   0)
WHITE = (255, 255, 255)
RED = (255,   0,   0)
GREEN = (0, 255,   0)
BLUE = (0,   0, 255)
CYAN = (50, 255, 255)
MAGENTA = (255, 0, 255)
YELLOW = (255, 255, 0)
ORANGE = (255, 127, 0)

bg = pygame.transform.scale(pygame.image.load("img/wp1864223-raspberry-pi-wallpapers.jpg"), (480, 320))
tempIcon = pygame.transform.scale(pygame.image.load("img/thermometer.png"), (48, 48))
humidityIcon = pygame.transform.scale(pygame.image.load("img/drops.png"), (48, 48))


class RectInfo(object):
    def __init__(self, rect):
        self.Width = rect.size[0]
        self.Height = rect.size[1]
        self.StartX = rect.x
        self.StartY = rect.y
        self.EndX = self.StartX + self.Width
        self.EndY = self.StartY + self.Height


class Widget(object):
    def __init__(self, ohc_id, margin=3):
        self.ohcId = ohc_id
        self.margin = margin
        self._temp = 0.0
        self._humi = 0.0
        self._height = 0
        self._width = 0
        self._header_color = WHITE
        self._no_reading = False
        self._no_reading_color = BLUE

    def update(self, temp, humi=0.0):
        """used to update displayed value of temperature and humidity"""
        self._temp = temp
        self._humi = humi
        self._no_reading = False

    def get_size(self):
        """Get object size. Return (width, height) Available only after drawing a widget"""
        return self._width, self._height

    def extend_info(self, extend_info: dict):
        if extend_info['is_heating']:
            self._header_color = RED
        else:
            self._header_color = WHITE

        if extend_info['value'] == 0.0:
            #self._temp = 0.0
            #self._humi = 0.0
            self._no_reading = True

    def _get_value_color(self):
        if self._no_reading:
            if self._no_reading_color == RED:
                self._no_reading_color = BLUE
            else:
                self._no_reading_color = RED

            return self._no_reading_color
        else:
            return WHITE

    def draw(self, display: pygame.surface.SurfaceType, x, y):
        font_color = self._get_value_color()
        header = RectInfo(display.blit(font.render(self.ohcId.split('/')[-1].capitalize(), 1, self._header_color),
                                       (x, y)))
        temp = RectInfo(display.blit(tempIcon, (header.StartX, header.EndY + self.margin)))
        tempLbl = RectInfo(display.blit(font.render("%.1f°C" % self._temp, 1, font_color),
                                        (temp.EndX + self.margin,
                                         temp.EndY - temp.Height / 2)))

        if self._humi > 0:
            humi = RectInfo(display.blit(humidityIcon, (temp.StartX, temp.EndY + self.margin)))
            humiLbl = RectInfo(display.blit(font.render("%.f" % self._humi + " %", 1, font_color),
                                            (humi.EndX + (self.margin * 2),
                                             humi.EndY - humi.Height / 2 - 3)))
            if self._height == 0 and self._width == 0:
                self._height = humiLbl.EndY
                self._width = max((header.Width, temp.EndX, tempLbl.EndX, humi.EndX, humiLbl.EndX)) + self.margin
        else:
            if self._height == 0 and self._width == 0:
                self._height = tempLbl.EndY
                self._width = max((header.Width, temp.EndX, tempLbl.EndX)) + self.margin
        pass


def on_disconnect(client, userdata, rc):
    global widgets
    widgets = []
    draw()

    while True:
        try:
            if client.reconnect() == 0:
                return

            time.sleep(0.5)
        except:
            pass


def on_connect(client, userdata, flags, rc):
    # logger.debug("Connected with result code "+str(rc))
    client.subscribe("ohc/sensors/#")
    client.subscribe("ohc/sensor_info")


def on_message(cli, userdata, msg):
    global client
    global widgets
    value = str(msg.payload.decode("utf-8"))

    if msg.topic == 'ohc/sensor_info':
        sensor_info = ast.literal_eval(value)
        widget = [w for w in widgets if w.ohcId == sensor_info['ohc_sensor']]
        widget.pop(0).extend_info(sensor_info)
        return

    if not any(w.ohcId == msg.topic for w in widgets):
        widgets.append(Widget(msg.topic))

    temp = float(value.split(';')[0])
    humi = float(value.split(';')[1])
    for w in widgets:
        if msg.topic == w.ohcId:
            w.update(temp, humi)


def draw():
    global widgets
    disp.blit(bg, (0, 0))
    location = Location(2, 0)
    for widget in widgets:
        widget.draw(disp, location.x, location.y)
        w, h = widget.get_size()
        location = Location(w, location.y)
        if location.x >= 430 and location.y == 0:
            # move to second line
            location = Location(2, widgets[0].get_size()[1] + 30)
        elif location.x >= 430 and location.y != 0:
            print("Not enough space to draw control, abort refresh")
            break


client = Client()
client.on_connect = on_connect
client.on_message = on_message
client.on_disconnect = on_disconnect

client.connect(config.MqttBroker)


game_running = True
while game_running:
    client.loop()
    for event in pygame.event.get():
        if event.type == QUIT:
            game_running = False
        
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                game_running = False

    draw()
    pygame.display.update()
    # TODO: Consider slowing it down.Maybe not the loop but display process
    time.sleep(0.2)

pygame.quit()
