import argparse
import time
import sys

from random import *
from paho.mqtt.client import Client


def parse_args(argv=sys.argv[1:]):
    parser = argparse.ArgumentParser(description="My Python script.")
    
    # Use the arg_* snippets to add arguments
    parser.add_argument("-l", "--location", type=str, metavar="ARG", help="Argument help.")

    return parser.parse_args(argv)

def on_disconnect(client, userdata, rc):
    while True:
        try:
            print("Trying to connect.....")
            if client.reconnect() == 0:
                return
            time.sleep(0.5)
        except:
            pass


if __name__ == "__main__":
    args = parse_args()
    print("Simulating room: %s" % args.location)

    client = Client()
    client.on_disconnect = on_disconnect
    client.connect("192.168.1.111")

    while True:
        temp = randrange(16, 30, 2)
        humi = randrange(25, 70, 2)
        print("Publishing temp: %s and humi %s"%(temp, humi))
        client.publish("ohc/sensors/%s" % args.location, "%s;%s" % (temp, humi))

        time.sleep(60.0)

